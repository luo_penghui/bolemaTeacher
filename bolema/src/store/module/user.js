// 开启严格模式
'user strict'
// 配置整个模块的导出对象
const user = {
  // 状态集的数据源
  state: {
    user: {
      account: '',
      name: '',
      userId: null,
      img: ''
    },
    isLogin: 0
  },
  // vuex给外界提供的获取函数
  getters: {
    user (state) {
      return state.user
    },
    isLogin (state) {
      return state.isLogin === 1
    }
  },
  // 外界操作get函数的函数
  // 同步请求
  mutations: {
    // 修改登录状态
    changeLoginState (state, flag) {
      state.isLogin = flag
    },
    // 存储用户的详细信息
    saveLoginInfo (state, user) {
      state.user.account = user.account
      state.user.name = user.name
      state.user.userId = user.userId
      state.user.img = user.head_img_url
    }
  },
  // 异步请求
  actions: {
    changeLoginState ({commit}) {
      commit('changeLoginState', 1)
    },
    saveLoginInfo ({commit}, user) {
      commit('saveLoginInfo', user)
    }
  }
}
export default user
