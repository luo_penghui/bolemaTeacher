import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/vue/Login'
import homepage from '@/vue/homepage'
import home from '@/vue/home'
import index from '@/vue/index'
import addExam from '@/vue/addExam'
import addCourse from '@/vue/addCourse'
import addPlan from '@/vue/addPlan'
import addSchedule from '@/vue/addSchedule'
import classManage from '@/vue/classManage'
import exam from '@/vue/exam'
import course from '@/vue/course'
import questionBank from '@/vue/questionBank'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'home',
      component: home,
      redirect: '/classManage',
      children: [
        {
          path: '/classManage',
          name: 'classManage',
          component: classManage
        },
        {
          path: '/exam',
          name: 'exam',
          component: exam
        },
        {
          path: '/course',
          name: 'course',
          component: course
        },
        {
          path: '/questionBank',
          name: 'questionBank',
          component: questionBank
        }
      ]
    },
    {
      path: '/homepage',
      name: 'homepage',
      component: homepage,
      redirect: '/index',
      children: [
        {
          path: '/index',
          name: 'index',
          component: index
        },
        {
          path: '/addExam',
          name: 'addExam',
          component: addExam
        },
        {
          path: '/addCourse',
          name: 'addCourse',
          component: addCourse
        },
        {
          path: '/addPlan',
          name: 'addPlan',
          component: addPlan
        },
        {
          path: '/addSchedule',
          name: 'addSchedule',
          component: addSchedule
        }
      ]
    }
  ]
})
